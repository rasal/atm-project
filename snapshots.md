App presentation

![welcome](/uploads/ea024126737566830140d9c8bce87f8a/welcome.png)

![login](/uploads/eacd19752ca2205e0f3fe795a1dc1aeb/login.png)

![menu](/uploads/7c7eaac532fb6bb0c7edd1c77fd4806b/menu.png)

![deposit](/uploads/d61dcd422da660768cde99ea4651da5e/deposit.png)

![withdraw](/uploads/051c372269f44b4280865e899130c1f4/withdraw.png)

![exchange](/uploads/7344edb2c8c998228dda59711ff20aed/exchange.png)

![rates](/uploads/639f364c5927211353088fa994d31d2c/rates.png)

# ATM - python project
___
### Configuration Guide:

Install all required dependencies:
```
pip install -r requirements.txt
```

In backend_kivy.py external file replace:
```
from distutils.version import LooseVersion

_mpl_ge_1_5 = LooseVersion(matplotlib.__version__) >= LooseVersion('1.5.0')
_mpl_ge_2_0 = LooseVersion(matplotlib.__version__) >= LooseVersion('2.0.0')
```
to
```
from packaging.version import Version

_mpl_ge_1_5 = Version(matplotlib.__version__) >= Version('1.5.0')
_mpl_ge_2_0 = Version(matplotlib.__version__) >= Version('2.0.0')
```

### WARNING
From time to time, used API related to currency exchange rates stop being supported, 
in such case you have to manually set up a new provider or get a paid version.

___
### Features:
1. You can sign in on account by enter four digit PIN (default = 1234, then you can change).
2. If you enter incorrect inputs or not allowed characters it will show appropriate alert.
3. In case you exceed 3 login attempts, the program will be automatically closed.
4. You can deposit and withdraw float amounts with 2 decimal up to 100,000 at once.
5. The possibility of having an account with a negative balance is blocked.
6. You can check your current account balance and transactions history at any time.
7. The menu is practical, you can move freely between operations.
8. There are chosen of 4 currencies to exchange with a commission fee form transactions.
9. The program have function of displaying the history of made transactions.
10. The program automatically converts commas to dots and lowercase letters to printed letters, <br>
which prevents accidental entering the wrong format of numbers or characters.
11. App have functionality of drawing graph for currency rates changes on timeline of past year.
12. App registered new account is automatically with encrypted password.
___
### Description:
- To integrate the database with MySQL (old_version) import the atm_db folder data by checking <br>
the 'dump' option in the admin panel and selecting the appropriate path.


- Libraries used in project:
bcrypt, json, kivy, matplotlib, pytest, re, requests, sqlite3, numpy
___
### Version history:
- **ALPHA** is dated on: 2021-02-10. Consist all functionalities except hashed passwords. <br>
Databases rely on SQL queries without injection security. Code was wrote in functional paradigm.


- **BETA** is dated on about December 2021, but from time to time can receive minor updates. <br>
Code is as well wrote in functional paradigm with slight hallmarks of object-oriented programming. <br>
New added features are hashed passwords, changing database to lighter form (sqlite), <br>
better limitation and control for input flow. However, the biggest makeover consist <br>
implementation of fully interactive GUI (kivy), with even possibility to see chart <br>
of actual and past currency exchange rate values.


Note:
Cannot make CI/CI test automation due to Kivy library deprecated methods.
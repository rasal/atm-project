```mermaid
graph TB

  subgraph "Python ATM App"
  Node1[WelcomeScreen] ==> Node2[LoginScreen]
  Node1[WelcomeScreen] --> Node3[RegisterScreen]
  Node1[WelcomeScreen] --> Node4[SupportScreen]
  Node1[WelcomeScreen] --> Node5[Exit]

  Node2[LoginScreen] --> Node6[StatusScreen]
  Node2[LoginScreen] --> Node7[HistoryScreen]
  Node2[LoginScreen] --> Node8[DepositScreen]
  Node2[LoginScreen] --> Node9[WithdrawScreen]
  Node2[LoginScreen] --> Node10[ExchangeMenuScreen]
  Node2[LoginScreen] --> Node11[ChangePinScreen]
  Node2[LoginScreen] --> Node12[Logout]

  Node10[ExchangeScreen] --> Node13[ExchangeOperationScreen]
  Node10[ExchangeScreen] --> Node14[ExchangeInformationScreen]
  Node10[ExchangeScreen] --> Node15[Back]
end

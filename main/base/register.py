from main.data.database import Database
from kivy.uix.screenmanager import Screen
from bcrypt import hashpw, gensalt
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.app import App
import random
import re


class RegisterWindow(Screen):
    def __init__(self, database, **kw):
        super(RegisterWindow, self).__init__(**kw)
        self.db = database

    def input_register_text(self):
        firstname = self.ids.firstname.text
        lastname = self.ids.lastname.text
        password = self.ids.password.text
        repeat_password = self.ids.repeat_password.text
        return firstname, lastname, password, repeat_password

    def reset_register_keys(self):
        self.ids.firstname.text = ""
        self.ids.lastname.text = ""
        self.ids.password.text = ""
        self.ids.repeat_password.text = ""

    def register_btn(self):
        firstname, lastname, password, repeat_password = self.input_register_text()
        if self.name_checker(firstname, lastname) \
                and self.password_creator(password) \
                and password == repeat_password:
            account_number = random.randint(100000, 200000)
            Database(self.db).add_user(
                firstname.capitalize(), lastname.capitalize(),
                account_number, self.hashed(password).decode('utf-8'))
            App.get_running_app().root.access_denied = False
            self.correct_register_popup(self.make_username(firstname, lastname))
            self.reset_register_keys()
            self.manager.current = "welcome_page"
        else:
            App.get_running_app().root.access_denied = True

    def name_checker(self, firstname, lastname):
        if firstname.isalpha() and lastname.isalpha():
            return True
        else:
            self.invalid_username_popup()
            return False

    def password_creator(self, password):
        if len(password) < 7:
            print("Your password must have at least 8 letters.")
            self.invalid_password_popup()
            return False
        elif re.search(r'[0-9]{4,}', password) is None:
            print("Your password must contain at least four numbers.")
            self.invalid_password_popup()
            return False
        elif re.search('[A-Z]', password) is None:
            print("Your password must have a capital letter.")
            self.invalid_password_popup()
            return False
        else:
            return True

    @staticmethod
    def random_account_number():
        return random.randint(10000, 20000)

    @staticmethod
    # for general purposes
    def hashed(password):
        return hashpw(password.encode('utf-8'), gensalt(16))

    @staticmethod
    def make_username(firstname, lastname):
        return firstname.capitalize() + " " + lastname.capitalize()

    @staticmethod
    def correct_register_popup(username):
        pop = Popup(title="Register successful",
                    content=Label(text=f"You can login now! {username}"),
                    size_hint=(None, None), size=(320, 180))
        pop.open()

    @staticmethod
    def invalid_username_popup():
        pop = Popup(title="Invalid Username",
                    content=Label(text="Username (name and surname)"
                                       "\nshould be from letters."),
                    size_hint=(None, None), size=(320, 180))
        pop.open()

    @staticmethod
    def invalid_password_popup():
        pop = Popup(title="Invalid Password",
                    content=Label(text="Password should be capitalize,"
                                       "\nhave at least 8 letters,"
                                       "\nand contain four digits."),
                    size_hint=(None, None), size=(320, 180))
        pop.open()

from kivy.uix.screenmanager import Screen
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.app import App


class LoginWindow(Screen):
    def __init__(self, database, **kw):
        super(LoginWindow, self).__init__(**kw)
        self.db = database

    def input_login_text(self):
        account_number = self.ids.account_number.text
        password = self.ids.password.text
        return account_number, password

    def reset_login_keys(self):
        self.ids.account_number.text = ""
        self.ids.password.text = ""

    def login_btn(self):
        account_number, password = self.input_login_text()
        self.reset_login_keys()
        if self.db.validate_login(account_number, password.encode("utf-8")):
            self.correct_login_popup()
            App.get_running_app().root.access_denied = False
            self.manager.current = "main_menu_page"
        else:
            # App.get_running_app().root.access_denied = True
            self.invalid_login_popup()
            App.get_running_app().root.access_denied = False
            self.manager.current = "main_menu_page"

    @staticmethod
    def get_username():
        # INFO - username from account_number to alert.
        return "Marek"

    def correct_login_popup(self):
        pop = Popup(title="Login successful",
                    content=Label(text=self.get_username()),
                    size_hint=(None, None), size=(320, 180),
                    auto_dismiss=True)
        pop.open()

    @staticmethod
    def invalid_login_popup():
        pop = Popup(title="Login failed",
                    content=Label(text="Invalid username or password."),
                    size_hint=(None, None), size=(320, 180),
                    auto_dismiss=True)
        pop.open()

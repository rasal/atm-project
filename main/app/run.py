from kivy.core.window import Window
from kivy.lang import Builder
from kivymd.app import MDApp
from kivy.uix.screenmanager import ScreenManager
from main.data.connector import sql_connector
from main.data.database import Database
from main.rest.welcome import WelcomeWindow
from main.base.login import LoginWindow
from main.rest.main_menu import MainMenuWindow
from main.ops.status import StatusWindow
from main.ops.history import HistoryWindow
from main.ops.deposit import DepositWindow
from main.ops.withdraw import WithdrawWindow
from main.ops.exchange.menu import ExchangeMenuWindow
from main.ops.exchange.operation import ExchangeOperationWindow
from main.ops.exchange.info import ExchangeInfoWindow
from main.ops.change_pin import ChangePinWindow
from main.base.register import RegisterWindow
from main.base.support import SupportWindow


class MainApp(MDApp):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def build(self):
        Window.borderless = False
        self.theme_cls.theme_style = "Dark"
        Builder.load_file("style.kv")
        sm = ScreenManager()
        db = Database(sql_connector())
        sm.add_widget(WelcomeWindow(name="welcome_page"))
        sm.add_widget(LoginWindow(name="login_page", database=db))
        sm.add_widget(MainMenuWindow(name="main_menu_page"))
        sm.add_widget(StatusWindow(name="status_page", database=db))
        sm.add_widget(HistoryWindow(name="history_page", database=db))
        sm.add_widget(DepositWindow(name="deposit_page", database=db))
        sm.add_widget(WithdrawWindow(name="withdraw_page", database=db))
        sm.add_widget(ChangePinWindow(name="change_pin_page", database=db))
        sm.add_widget(SupportWindow(name="support_page"))
        sm.add_widget(RegisterWindow(name="register_page", database=db))
        sm.add_widget(ExchangeMenuWindow(name="exchange_menu_page"))
        sm.add_widget(ExchangeOperationWindow(name="exchange_operation_page", database=db))
        sm.add_widget(ExchangeInfoWindow(name="exchange_info_page", database=db))
        return sm


if __name__ == '__main__':
    app = MainApp()
    app.run()

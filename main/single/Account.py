from config import config
from main.data.connector import sql_connector
from main.data.database import Database


class Account:
    number = 1234560

    def __init__(self, firstname, lastname, balance):
        self.firstname = firstname
        self.lastname = lastname
        self.__balance = balance
        self.account = {}

    def add_user(self, **kwargs):
        self.number += 1
        self.account.update(
            {self.number: kwargs})

    def deposit(self, amount):
        self.__balance += amount

    def withdraw(self, amount):
        if self.__balance < amount:
            print("Insufficient funds.")
            return False
        self.__balance -= amount
        print("Withdraw ops successful.")
        return True

    @property
    def get_balance(self):
        return str(self.__balance)

    def get_info(self):
        return self.account

    @staticmethod
    def whitespace_number(number):
        """
        @ FUNCTION DESCRIPTION:
        1. Take an input number
        2. Make for loop:
         - in range
         - from 0 to len(var)
         - in interval of 3 steps
        3. As result:
         - revert number - var[::-1]
         - measure every 3th part - var[i:i + 3]
        4. Revert back string to normal state
        5. Join commas into created whitespaces
        6. Return end variable
        """
        return ",".join(number[::-1][i:i + 3] for i in range(0, len(number), 3))[::-1]


if __name__ == "__main__":
    account1 = Account(config.account_data(Database(sql_connector()))[0][1],
                       config.account_data(Database(sql_connector()))[0][2],
                       config.balance_data(Database(sql_connector()))[0][1])

    """TEST 1 - BASIC OPERATION."""
    # account2 = Account("Ralph", "Lauren", 20)
    #
    # print(account2.firstname)  # Ralph
    # print(account2.get_balance)  # balance
    #
    # amount = (round(float(input("Insert amount to deposit: ").replace(',', '.')), 2))
    # account2.deposit(amount)
    # print(account2.get_balance)  # balance
    #
    # amount = (round(float(input("Insert amount to withdraw: ").replace(',', '.')), 2))
    # account2.withdraw(amount)
    # print(account2.get_balance)  # balance
    """"END"""

    """TEST 2 - LOADING EXISTED ACCOUNT FROM DB."""
    # print(account1.firstname, account1.lastname)
    # account_1_balance = account1.get_balance
    # print("1.", account_1_balance)
    #
    # account_1_withdraw = account1.withdraw(100)
    # print("2.", account_1_withdraw)
    #
    # print(account1.get_info(), "info")
    # print(account1.get_balance, "balance after withdraw")
    """END"""

    """TEST 3 - CREATE NEW ACCOUNT."""
    print("-" * 200)
    account3 = Account(config.account_data(Database(sql_connector()))[0][1],
                       config.account_data(Database(sql_connector()))[0][2],
                       Account.whitespace_number(str(356578)))

    print(account3.firstname, account3.lastname, account3.get_balance + "$\n")

    account3.add_user(firstname="Michael", lastname="Schumacher",
                      password="xxxx", balance=1000000, date="20.02.2020")

    account3.add_user(firstname="Tom", password="yyyy",
                      lastname="Cruise", balance=200000, date="20.06.2022")

    print(account3.account)
    print(account3.account[1234562])
    """END"""

# Perhaps FirstName and Lastname should be in one column

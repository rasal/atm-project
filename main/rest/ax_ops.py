from dateutil.relativedelta import relativedelta
from kivy.uix.label import Label
from kivy.uix.popup import Popup
import datetime


class AuxiliaryOperation:
    @staticmethod
    def get_datetime():
        return str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))

    @staticmethod
    def get_date():
        return str(datetime.datetime.now()).split(" ")[0]

    @staticmethod
    def previous_year():
        return str((datetime.datetime.now()-relativedelta(years=1)).strftime("%Y-%m-%d"))

    @staticmethod
    def previous_twelve_months():
        result = []
        for months in range(12):
            previous_months = str((datetime.datetime.now()-relativedelta(months=months)).strftime("%Y-%m-%d"))
            result.append(previous_months)
        return result

    @staticmethod
    def amount_from_value(value):
        return round(float(value.replace(',', '.')), 2)

    @staticmethod
    def not_valid_number():
        pop = Popup(title="Not a valid number",
                    content=Label(text="The amount is not a valid number."),
                    size_hint=(None, None), size=(320, 180),
                    auto_dismiss=True)
        pop.open()

    @staticmethod
    def incorrect_value():
        pop = Popup(title="Incorrect value",
                    content=Label(text="Sorry, incorrect value."),
                    size_hint=(None, None), size=(320, 180),
                    auto_dismiss=True)
        pop.open()

    @staticmethod
    def insufficient_funds():
        pop = Popup(title="Incorrect value",
                    content=Label(text="Sorry, you do not have enough money."),
                    size_hint=(None, None), size=(320, 180),
                    auto_dismiss=True)
        pop.open()

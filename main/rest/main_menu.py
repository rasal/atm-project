from kivy.properties import ObjectProperty, BooleanProperty
from kivy.uix.screenmanager import Screen


class MainMenuWindow(Screen):
    access_denied = BooleanProperty(True)

    n = ObjectProperty(None)
    created = ObjectProperty(None)
    email = ObjectProperty(None)
    current = ""

    @staticmethod
    def logout():
        Screen.current = "welcome_page"

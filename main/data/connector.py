import sqlite3
from config import data


def sql_connector():
    connection = sqlite3.connect(data)
    connection.isolation_level = None
    return connection

from main.rest.ax_ops import AuxiliaryOperation
import sqlite3


class Database:
    def __init__(self, connection):
        self.cursor = connection.cursor()

    # def create_table(self, sql: str):
    #     self.cursor.execute(sql)
    #     self.connection.commit()

    # def insert(self, table, *values):
    #     self.cursor.execute(f"INSERT INTRO {table} VALUES {values}, values")
    #     self.connection.commit()

    def fetch_data(self, table, values, *condition):
        """Collect data from database and put into list."""
        records = self.cursor.execute(f"SELECT {values} FROM {table}")
        return [elements for elements in records]

    def get_user(self, account_number, password):
        records = self.cursor.execute(
            "SELECT account_number, password FROM accounts "
            "WHERE account_number = ? AND password = ?",
            (account_number, password,))
        return [elements for elements in records]

    def validate_login(self, account_number, password):
        """Checking if account number and password suits each other."""
        if self.get_user(account_number, password):
            return True
        else:
            return False

    def deposit(self, amount):
        """Assign amount in database."""
        self.cursor.execute(f"UPDATE balance SET USD = USD + {amount}")
        self.cursor.execute("INSERT INTO transactions (type, USD, date) VALUES (?, ?, ?)",
                            ("deposit", amount, AuxiliaryOperation.get_datetime()))

    def withdraw(self, amount):
        """Extract amount from database."""
        if self.available_funds(amount):
            self.cursor.execute(f"UPDATE balance SET USD = USD - {amount}")
            self.cursor.execute("INSERT INTO transactions (type, USD, date) VALUES (?, ?, ?)",
                                ("withdraw", amount, AuxiliaryOperation.get_datetime()))
            return True
        else:
            return False

    def available_funds(self, amount):
        """Checking if there is enough money on the account for the transaction."""
        self.cursor.execute("SELECT USD FROM balance")
        for USD in self.cursor:
            if USD[0] >= amount:
                return True
            else:
                return False

    # ------------------------------------------------------------------------------------------------

    def add_user(self, name, surname, account_number, password):
        try:
            self.cursor.execute("INSERT INTO accounts (firstname, lastname, account_number, password, created_on)"
                                "VALUES (?, ?, ?, ?, ?)",
                                (name, surname, account_number, password, AuxiliaryOperation.get_datetime()))
            return True
        except sqlite3.Error:
            return False

    # ---------------------------------------------------------------------------------------------------

    def exchange_currency(self, base, amount):
        """Checking if there is enough currency for the transaction."""
        status = f"SELECT {base} FROM balance"
        self.cursor.execute(status)
        for funds in self.cursor:
            if funds[0] >= amount:
                return True
            else:
                return False

    def exchange_withdraw(self, base, amount):
        """Extract exchange amount from database."""
        self.cursor.execute(f"UPDATE balance SET {base} = {base} - {amount}")
        self.cursor.execute(f"INSERT INTO transactions (type, {base}, date)"
                            f"VALUES ('ex_with', {amount}, '{AuxiliaryOperation.get_datetime()}')")

    def exchange_deposit(self, currency, conversion):
        """Assign exchange amount in database."""
        self.cursor.execute(f"UPDATE balance SET {currency} = ({currency} + {round(conversion, 2)})")
        self.cursor.execute(f"INSERT INTO transactions (type, {currency}, date)"
                            f"VALUES ('ex_depo', {round(conversion, 2)}, '{AuxiliaryOperation.get_datetime()}')")

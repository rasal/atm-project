from main.rest.ax_ops import AuxiliaryOperation
from kivy.properties import ObjectProperty
from kivy.uix.screenmanager import Screen
from kivy.uix.label import Label
from kivy.uix.popup import Popup
import re


class ExchangeOperationWindow(Screen):
    COMMISSION_CONSTANT = 1.2
    exchange_input = ObjectProperty(None)

    def __init__(self, database, **kw):
        super().__init__(**kw)
        self.db = database
        self.decision = None

    def on_enter(self):
        exchange_menu = self.manager.get_screen("exchange_menu_page")
        self.decision = exchange_menu.decision

    @staticmethod
    def successful_exchange(amount, base, conversion, to, provision):
        pop = Popup(title="Exchange successful",
                    content=Label(text=f"Operation succeed,"
                                       f"\nyour funds have exchanged"
                                       f"\nfrom {amount} {base}"
                                       f"\nto {round(conversion, 2)} {to}"
                                       f"\nwith provision {provision} {base}."),
                    size_hint=(None, None), size=(320, 180),
                    auto_dismiss=True)
        pop.open()

    @staticmethod
    def negative_exchange():
        pop = Popup(title="Negative exchange",
                    content=Label(text="Sorry, exchange amount"
                                       "\ncannot be on minus."),
                    size_hint=(None, None), size=(320, 180),
                    auto_dismiss=True)
        pop.open()

    @staticmethod
    def empty_exchange():
        pop = Popup(title="Empty exchange",
                    content=Label(text="Sorry, you cannot exchange nothing."),
                    size_hint=(None, None), size=(320, 180),
                    auto_dismiss=True)
        pop.open()

    @staticmethod
    def exceed_exchange():
        pop = Popup(title="Exceed exchange",
                    content=Label(text="Sorry, your amount exceed"
                                       "\nmaximum for transaction"
                                       "\nof 100 000$"),
                    size_hint=(None, None), size=(320, 180),
                    auto_dismiss=True)
        pop.open()

    def exchange_action(self):
        base, to, rate = self.decision
        value = self.ids.exchange_input.text
        self.ids.exchange_input.text = ""
        if re.findall('[0-9]+', value):
            try:
                self.exchange_checker(
                    base, to, float(rate), value)
            except ValueError:
                AuxiliaryOperation.not_valid_number()
        else:
            AuxiliaryOperation.incorrect_value()

    def exchange_checker(self, base, to, rate, value):
        amount = AuxiliaryOperation.amount_from_value(value)
        if 0.01 <= amount <= 100000:
            if self.db.exchange_currency(base, amount):
                self.exchange_funds(base, to, rate, amount)
            else:
                AuxiliaryOperation.insufficient_funds()
        elif amount < 0:
            self.negative_exchange()
        elif amount == 0:
            self.empty_exchange()
        elif amount > 100000:
            self.exceed_exchange()

    def exchange_funds(self, base, to, rate, amount):
        """Finalizing currency exchange and collection a provision fee from transaction."""
        provision = 0.8 * amount / 100 + self.COMMISSION_CONSTANT
        conversion = rate * (amount - provision)
        self.db.exchange_withdraw(base, amount)
        self.db.exchange_deposit(to, conversion)
        self.successful_exchange(
            round(amount, 2), base, round(conversion, 2), to, round(provision, 2))

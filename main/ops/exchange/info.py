from main.ops.exchange.menu import ExchangeMenuWindow
from pdfkivygui.garden import FigureCanvasKivyAgg
from main.rest.ax_ops import AuxiliaryOperation
from kivy.uix.screenmanager import Screen
from matplotlib import pyplot as plt


class ExchangeInfoWindow(Screen):
    def __init__(self, database, **kw):
        super().__init__(**kw)
        self.db = database
        self.decision = None

    def on_enter(self):
        exchange_menu = self.manager.get_screen("exchange_menu_page")
        self.decision = exchange_menu.decision
        base, to, z = self.decision
        chart_box = self.ids.chart_box
        chart_box.clear_widgets()
        address = self.timeseries_address(base)
        rates = ExchangeMenuWindow.exchange_connector(address)
        list_date, list_rates = self.data_parser(rates)
        sorted_rates = self.data_sorting(list_rates, to)
        chart_box.add_widget(FigureCanvasKivyAgg(
            self.diagram_properties(list_date, sorted_rates, base, to).gcf()))

    @staticmethod
    def timeseries_address(base):
        address = "timeseries?start_date={start_date}&end_date={end_date}&base=" \
                  "{base}&places=2&symbols=USD,EUR,GBP,PLN&amount=100" \
            .format(start_date=AuxiliaryOperation.previous_year(),
                    end_date=AuxiliaryOperation.get_date(), base=base)
        return address

    @staticmethod
    def diagram_properties(list_date, sorted_rates, base, to):
        print(base, to, sorted_rates)
        plt.style.use("dark_background")
        plt.xlabel("dates")
        plt.ylabel("rates")
        plt.title(f"Currency rates diagram "
                  f"\n(100 {base} as a base)", fontsize=18)
        plt.plot(list_date, sorted_rates, label=to, linewidth=4, linestyle="dashdot")
        plt.legend()
        return plt

    @staticmethod
    def data_parser(rates):
        list_date = []
        list_rates = []
        for no, values in enumerate(rates.items()):
            if no % 30 == 0:
                months = values[0][2:7][3:]
                years = values[0][2:7][:2]
                list_date.append(f"{months}/{years}")
                list_rates.append(values[1])
        return list_date, list_rates

    @staticmethod
    def data_sorting(list_rates, to):
        return [values[to] for values in list_rates]

from kivy.uix.screenmanager import Screen
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.app import App
import requests
import json


class ExchangeMenuWindow(Screen):
    CURRENCIES = ["USD", "EUR", "GBP", "PLN"]

    def __init__(self, **kw):
        super().__init__(**kw)
        self.decision = None

    @staticmethod
    def convert_address(base, to):
        return f"convert?from={base}&to={to}"

    @staticmethod
    def exchange_connector(address):
        url = f"https://api.exchangerate.host/{address}"
        response = requests.get(url)
        data = response.text
        parsed = json.loads(data)
        if url.__contains__("convert"):
            rates = parsed["info"]["rate"]
        else:
            rates = parsed['rates']
        return rates

    @staticmethod
    def cannot_be_the_same():
        pop = Popup(title="Currencies cannot be the same",
                    content=Label(text="Base and exchange currencies"
                                       "\ncannot be the same."),
                    size_hint=(None, None), size=(320, 180),
                    auto_dismiss=True)
        pop.open()

    @staticmethod
    def not_chosen_currencies():
        pop = Popup(title="Not chosen currencies",
                    content=Label(text="You have not choose currencies."),
                    size_hint=(None, None), size=(320, 180),
                    auto_dismiss=True)
        pop.open()

    @staticmethod
    def info_exchange(base, to, rate):
        pop = Popup(title="Exchange rate information",
                    content=Label(text=f"Conversion rate for"
                                       f"\n100 {base} is {round(100 * rate, 2)} {to}"
                                       f"\nplus provision from base with"
                                       f"\n1.2 {base} + 0,8% from transaction."),
                    size_hint=(None, None), size=(320, 180),
                    auto_dismiss=True)
        pop.open()

    def exchange_initializer(self):
        """Establishing a connection with the currency exchange rate database."""
        base = self.ids.btn1.text
        to = self.ids.btn2.text
        if base and to not in ["From: ", "To: "]:
            if base != to:
                url = self.convert_address(base, to)
                rates = self.exchange_connector(url)
                App.get_running_app().root.access_denied = False
                self.manager.current = "exchange_operation_page"
                self.decision = base, to, rates
            else:
                self.cannot_be_the_same()
                App.get_running_app().root.access_denied = True
        else:
            self.not_chosen_currencies()
            App.get_running_app().root.access_denied = True

    def exchange_info(self):
        """Establishing a connection with the currency exchange rate database."""
        base = self.ids.btn1.text
        to = self.ids.btn2.text
        if base and to not in ["From: ", "To: "]:
            if base != to:
                url = self.convert_address(base, to)
                rates = self.exchange_connector(url)
                App.get_running_app().root.access_denied = False
                self.manager.current = "exchange_info_page"
                self.decision = base, to, rates
            else:
                self.cannot_be_the_same()
                App.get_running_app().root.access_denied = True
        else:
            self.not_chosen_currencies()
            App.get_running_app().root.access_denied = True

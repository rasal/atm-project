from main.rest.ax_ops import AuxiliaryOperation
from kivy.properties import ObjectProperty
from kivy.uix.screenmanager import Screen
from kivy.uix.popup import Popup
from kivy.uix.label import Label
import re


class DepositWindow(Screen):
    deposit_input = ObjectProperty(None)

    def __init__(self, database, **kw):
        super(DepositWindow, self).__init__(**kw)
        self.db = database

    @staticmethod
    def successful_deposit(amount):
        pop = Popup(title="Deposit successful",
                    content=Label(text="Operation succeed,"
                                       "\nyour funds have increased"
                                       "\nby {}$".format(amount)),
                    size_hint=(None, None), size=(320, 180),
                    auto_dismiss=True)
        pop.open()
        return amount

    @staticmethod
    def negative_deposit():
        pop = Popup(title="Negative deposit",
                    content=Label(text="Sorry, deposit amount"
                                       "\ncannot be on minus."),
                    size_hint=(None, None), size=(320, 180),
                    auto_dismiss=True)
        pop.open()

    @staticmethod
    def empty_deposit():
        pop = Popup(title="Empty deposit",
                    content=Label(text="Sorry, you cannot deposit nothing."),
                    size_hint=(None, None), size=(320, 180),
                    auto_dismiss=True)
        pop.open()

    @staticmethod
    def exceed_deposit():
        pop = Popup(title="Exceed deposit",
                    content=Label(text="Sorry, your amount exceed"
                                       "\nmaximum for transaction"
                                       "\nof 100 000$"),
                    size_hint=(None, None), size=(320, 180),
                    auto_dismiss=True)
        pop.open()

    def deposit_action(self):
        value = self.deposit_input.text
        self.deposit_input.text = ""
        if re.findall('[0-9]+', value):
            try:
                self.deposit_checker(value)
            except ValueError:
                AuxiliaryOperation.not_valid_number()
        else:
            AuxiliaryOperation.incorrect_value()

    def deposit_checker(self, value):
        """
        Fetch string value,
        return float value.
        """
        amount = AuxiliaryOperation.amount_from_value(value)
        if 0.01 <= amount <= 100000:
            self.db.deposit(amount)
            return self.successful_deposit(amount)
        elif amount < 0:
            return self.negative_deposit()
        elif amount == 0:
            return self.empty_deposit()
        elif amount > 100000:
            return self.exceed_deposit()

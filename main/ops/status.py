from kivy.uix.boxlayout import BoxLayout
from kivymd.uix.datatables import MDDataTable
from kivymd.uix.screen import Screen
from config import config
from kivy.metrics import dp


class StatusWindow(Screen):
    def __init__(self, database, **kw):
        super(StatusWindow, self).__init__(**kw)
        self.db = database
        self.data = config.account_data(self.db)
        self.balance = config.balance_data(self.db)

    def on_enter(self):
        """After entering StatusWindow Screen thanks for
        on_enter method status will refresh automatically."""
        box = BoxLayout(padding=20)
        table = MDDataTable(
            pos_hint={'center_x': 0.5, 'center_y': 0.5},
            size_hint=(1, 0.6),
            use_pagination=False,
            column_data=[
                ("Account Name", dp(28)),
                ("USD", dp(28)),
                ("EUR", dp(28)),
                ("GBP", dp(28)),
                ("PLN", dp(28))
            ],
            row_data=[
                (f"{self.data[0][1]} {self.data[0][2]}",
                 f"{round(self.balance[0][1], 2)}$", f"{round(self.balance[0][2], 2)}€",
                 f"{round(self.balance[0][3], 2)}£", f"{round(self.balance[0][4], 2)}zł")])
        box.add_widget(table)
        self.add_widget(box)
        # status control action
        print("Real decimal value is", config.balance_data(self.db)[0][1])

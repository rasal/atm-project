from kivy.properties import ObjectProperty
from kivy.uix.screenmanager import Screen


class ChangePinWindow(Screen):
    account_number = ObjectProperty(None)
    password = ObjectProperty(None)

    def __init__(self, database, **kw):
        super(ChangePinWindow, self).__init__(**kw)
        self.db = database

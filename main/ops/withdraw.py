from kivy.properties import ObjectProperty
from main.rest.ax_ops import AuxiliaryOperation
from kivy.uix.screenmanager import Screen
from kivy.uix.popup import Popup
from kivy.uix.label import Label
from main.data.database import Database
import re


class WithdrawWindow(Screen):
    withdraw_input = ObjectProperty(None)

    def __init__(self, database, **kw):
        super(WithdrawWindow, self).__init__(**kw)
        self.db = database

    @staticmethod
    def successful_withdraw(amount):
        pop = Popup(title="Withdraw successful",
                    content=Label(text="Operation succeed,"
                                       "\nyou have withdrawn."
                                       "\n{}$".format(amount)),
                    size_hint=(None, None), size=(320, 180),
                    auto_dismiss=True)
        pop.open()
        return amount

    @staticmethod
    def negative_withdraw():
        pop = Popup(title="Negative withdraw",
                    content=Label(text="Sorry, withdraw amount"
                                       "\ncannot be on minus."),
                    size_hint=(None, None), size=(320, 180),
                    auto_dismiss=True)
        pop.open()

    @staticmethod
    def empty_withdraw():
        pop = Popup(title="Empty withdraw",
                    content=Label(text="Sorry, you cannot withdraw nothing."),
                    size_hint=(None, None), size=(320, 180),
                    auto_dismiss=True)
        pop.open()

    @staticmethod
    def exceed_withdraw():
        pop = Popup(title="Exceed withdraw",
                    content=Label(text="Sorry, your withdraw exceed"
                                       "\nmaximum for transaction"
                                       "\nof 100 000$"),
                    size_hint=(None, None), size=(320, 180),
                    auto_dismiss=True)
        pop.open()

    def withdraw_action(self):
        value = self.withdraw_input.text
        self.withdraw_input.text = ""
        if re.findall('[0-9]+', value):
            try:
                self.withdraw_checker(value)
            except ValueError:
                AuxiliaryOperation.not_valid_number()
        else:
            AuxiliaryOperation.incorrect_value()

    def withdraw_checker(self, value):
        amount = AuxiliaryOperation.amount_from_value(value)
        if 0.01 <= amount <= 100000:
            if self.db.withdraw(amount):
                return self.successful_withdraw(amount)
            else:
                return AuxiliaryOperation.insufficient_funds()
        elif amount < 0:
            return self.negative_withdraw()
        elif amount == 0:
            return self.empty_withdraw()
        elif amount > 100000:
            return self.exceed_withdraw()

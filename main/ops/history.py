from kivy.uix.boxlayout import BoxLayout
from kivy.uix.screenmanager import Screen
from kivymd.uix.datatables import MDDataTable
from config import config
from kivy.metrics import dp


class HistoryWindow(Screen):
    def __init__(self, database, **kw):
        super(HistoryWindow, self).__init__(**kw)
        self.db = database

    def on_enter(self):
        """After entering HistoryWindow Screen thanks for
        on_enter method status will refresh automatically"""
        box = BoxLayout(padding=20)
        table = MDDataTable(
            pos_hint={'center_x': 0.5, 'center_y': 0.5},
            size_hint=(1, 0.6),
            use_pagination=True,
            column_data=[
                ("No", dp(28)),
                ("type", dp(28)),
                ("USD", dp(28)),
                ("EUR", dp(28)),
                ("GBP", dp(28)),
                ("PLN", dp(28)),
                ("Date", dp(28))
            ])
        table.row_data = config.transaction_data(self.db)
        box.add_widget(table)
        self.add_widget(box)

        # first table for deposit/withdraw
        # second table for exchange
        # sum of transactions amount

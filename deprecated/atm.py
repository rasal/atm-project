# By: Adam Kurek
# Date: 2021-02-10
# File Name: ATM project
# Description: bank project


from prettytable import PrettyTable
from termcolor import colored
from datetime import datetime
import mysql.connector
import requests
import json

connection = mysql.connector.connect(
    user='root', password='*****', host='localhost',
    database='python_db', auth_plugin='mysql_native_password')

cursor = connection.cursor()

CURRENCIES = ['USD', 'EUR', 'GBP', 'PLN']


def timer():
    """Shows the current date and time."""
    return datetime.now().strftime("%Y-%m-%d %H:%M:%S")


# ____________________________________________  LOGIN  _____________________________________________


def welcome_panel(current_pin):
    """Starting text."""
    print('\n' + '*' * 26 + '  WELCOME TO PYTHON ATM  ' + '*' * 25,
          '\nTo continue please enter your password: '
          f'\n(you have 3 attempts to use)')
    login(current_pin)


def get_pin_number():
    """Checking if PIN input is an integer."""
    while True:
        try:
            return int(input('\nEnter PIN: '))
        except ValueError:
            print(colored('It is not an integer.', 'red'))


def login(current_pin, attempts=3):
    """Login attempts counter."""
    print('\n' + '-' * 34 + '  LOGIN  ' + '-' * 33)
    while attempts > 0:
        if not validate_pin(current_pin):
            attempts -= 1
    else:
        quit('Your card has been blocked.')


def validate_pin(current_pin):
    """Validation of the PIN."""
    pin = get_pin_number()
    if pin == current_pin:
        print(colored('Login successful!', 'blue'))
        menu()
    else:
        if pin in range(1000, 10000):
            print(colored('PIN is incorrect.', 'red'))
        else:
            print(colored('PIN must have 4 digits.', 'red'))


# ____________________________________________  MENU  ______________________________________________


def menu():
    """Function dashboard."""
    print('\n' + '-' * 33, '  MENU  ', '-' * 33)
    print('Please choose ops:'
          '\n1) status\n2) history\n3) deposit\n4) withdraw\n5) exchange\n6) new PIN\n7) exit')
    while True:
        operation = get_operation_number()
        """Choosing ops from function dashboard."""
        if operation == 1:
            database_status()
        elif operation == 2:
            history()
        elif operation == 3:
            deposit()
        elif operation == 4:
            withdraw()
        elif operation == 5:
            exchange_connector()
        elif operation == 6:
            change_pin()
        elif operation == 7:
            quit('Thank you for using our service, see you next time.')
        else:
            print(colored('Please choose correct number of ops.', 'red'))


def get_operation_number():
    """Checking if ops input is an integer."""
    while True:
        try:
            return int(input('\nOperation: '))
        except ValueError:
            print(colored('Operation must be an integer.', 'red'))


# __________________________________________  OPERATION  __________________________________________


def deposit():
    """Transferring the value of the deposit into the database."""
    print('\n' + '-' * 32, '  DEPOSIT  ', '-' * 31)
    print('Select the amount you would like to deposit:'
          '\nThe limit of one-time ops is 10,000.'
          '\n(press 0 to return to the previous menu)\n')
    while True:
        amount = get_amount_number()
        if 0.01 < amount < 10000.01:
            database_deposit(amount)
            print(colored(f'Operation succeed, your funds have increased by {amount}$.\n', 'blue'))
            menu()
        elif amount == 0:
            menu()
        else:
            print(colored('Select the appropriate amount available for deposit.', 'red'))


def withdraw():
    """Extracting the amount of the withdrawal value from the database."""
    print('\n' + '-' * 31, '  WITHDRAW  ', '-' * 31)
    print('Select the amount you would like to withdraw:'
          '\nThe limit of one-time ops is 10,000.'
          '\n(press 0 to return to the previous menu)\n')
    while True:
        amount = get_amount_number()
        print(amount)
        if 0.00 < amount < 10000.01:
            available_funds(amount)
        elif amount == 0:
            menu()
        else:
            print(colored('Select the appropriate amount available for withdraw.', 'red'))
            withdraw()


def get_amount_number():
    """Checking if amount input is an integer."""
    while True:
        try:
            return round(float(input('\nAmount: ').replace(',', '.')), 2)
        except ValueError:
            print(colored('The amount is not a valid number.', 'red'))


def available_funds(amount):
    """Checking if there is enough money on the account for the transaction."""
    status = 'SELECT USD FROM balance'
    cursor.execute(status)
    for USD in cursor:
        if USD[0] >= amount:
            database_withdraw(amount)
            print(colored(f'Operation succeed, you have withdrawn {amount}$.\n', 'blue'))
            menu()
        else:
            print(colored('You do not have enough money.\n', 'red'))
            withdraw()


def database_deposit(amount):
    """Assign amount in database."""
    deposit1 = f'UPDATE balance SET USD = (SELECT SUM(USD + {amount}));'
    deposit2 = f'INSERT INTO transactions (ID, type, USD, date) ' \
               f'VALUES (1, "deposit", {amount}, "{timer()}");'
    cursor.execute(deposit1)
    cursor.execute(deposit2)
    connection.commit()


def database_withdraw(amount):
    """Extract amount from database."""
    withdraw1 = f'UPDATE balance SET USD = (SELECT SUM(USD - {amount}));'
    withdraw2 = f'INSERT INTO transactions (ID, type, USD, date)' \
                f'VALUES (1, "withdraw", {amount}, "{timer()}");'
    cursor.execute(withdraw1)
    cursor.execute(withdraw2)
    connection.commit()


# __________________________________________  EXCHANGE  _________________________________________


def base_input():
    """Getting "base" input."""
    base = input('\nConvert from: ').upper()
    return base


def to_input():
    """Getting "to" input."""
    to = input('\nConvert to: ').upper()
    return to


def exchange_connector():
    """Establishing a connection with the currency exchange rate database."""
    print('\n' + '-' * 27, '  CURRENCY EXCHANGE  ', '-' * 26)
    print('Available currencies: ')
    print(' '.join(CURRENCIES))
    print('The limit of one-time ops is 8,000.')
    print('(press 0 to return to the previous menu)\n')
    base = base_input()
    if base in CURRENCIES:
        # https://exchangeratesapi.io/
        url = f'https://api.exchangeratesapi.io/latest?base={base}'
        response = requests.get(url)
        data = response.text
        parsed = json.loads(data)
        rates = parsed['rates']
        counter_date = parsed['date']
        exchange_rates(base, rates, counter_date)
    elif base == '0':
        menu()
    else:
        print(colored('There is no such currency in the database.\n', 'red'))
        exchange_connector()


def exchange_rates(base, rates, counter_date):
    """Checking the exchange rate."""
    print(f'\nConversion rate per 100 {base}')
    print('-' * 60)
    CURRENCIES.remove(base)
    for currency, rate in rates.items():
        if currency.strip().upper() in CURRENCIES:
            print(f'{currency} = {round(100 * rate, 4)}')
    print('\ndate:', counter_date)
    print('-' * 60)
    CURRENCIES.append(base)
    currency_checker(base, rates)


def currency_checker(base, rates):
    """Checking and comparing the choice of currencies."""
    to = to_input()
    while base != to:
        if to in rates:
            exchange_completion(base, to, rates)
        elif to == '0':
            menu()
        else:
            print(colored('There is no such currency in the database.\n', 'red'))
            exchange_connector()
    print(colored('You can not convert the same currencies.\n', 'red'))
    exchange_connector()


def exchange_completion(base, to, rates):
    """Finalization of the currency exchange process."""
    print(f'You want to exchange from {base} to {to}.')
    amount = get_amount_number()
    if 0.00 < amount < 8000.01:
        available_currency(base, to, rates, amount)
    elif amount == 0:
        menu()
    else:
        print(colored('Select the appropriate amount available for exchange.', 'red'))
        exchange_connector()


def available_currency(base, to, rates, amount):
    """Checking if there is enough currency for the transaction."""
    status = f'SELECT {base} FROM balance'
    cursor.execute(status)
    for funds in cursor:
        if funds[0] >= amount:
            exchange_funds(base, to, rates, amount)
        else:
            print(colored('You do not have enough money.\n', 'red'))
            exchange_connector()


def exchange_funds(base, to, rates, amount):
    """Checking if there is enough money on the account for the transaction."""
    for currency, rate in rates.items():
        if currency == to:
            conversion = rate * amount
            print('\nConversion rate for 100', base, 'is', round(100 * rate, 2), currency)
            print('-' * 60)
            print(colored(f'Your exchange rate is from {amount} {base} to '
                          f'{round(conversion, 2)} {currency}.', 'blue'))
            exchange_withdraw(base, amount)
            commission(amount, base, rate, currency)


def commission(amount, base, rate, currency):
    """Collection of a provision fee on currency exchange."""
    commission_constant = 0.2
    provision = 0.8 * amount / 100 + commission_constant
    print(colored(f'Commission fee = {round(provision, 2)} {base} (0,2 + 0,8% from transaction)', 'yellow'))
    amount = amount - provision
    conversion = rate * amount
    exchange_deposit(currency, conversion)
    print(colored(f'\nOperation succeed, your amount of exchange after provision fee is '
                  f'{round(conversion, 2)} {currency}.', 'green'))
    menu()


def exchange_withdraw(base, amount):
    """Extract exchange amount from database."""
    exchange_withdraw1 = f'UPDATE balance SET {base} = (SELECT SUM({base} - {amount}))'
    exchange_withdraw2 = f'INSERT INTO transactions (ID, type, {base}, date)' \
                         f'VALUES (1, "ex_with", {amount}, "{timer()}")'
    cursor.execute(exchange_withdraw1)
    cursor.execute(exchange_withdraw2)
    connection.commit()


def exchange_deposit(currency, conversion):
    """Assign exchange amount in database."""
    exchange_deposit1 = f'UPDATE balance SET {currency} ' \
                        f'= (SELECT SUM({currency} + {round(conversion, 2)}))'
    exchange_deposit2 = f'INSERT INTO transactions (ID, type, {currency}, date)' \
                        f'VALUES (1, "ex_depo", {round(conversion, 2)}, "{timer()}")'
    cursor.execute(exchange_deposit1)
    cursor.execute(exchange_deposit2)
    connection.commit()


# ________________________________________  TRANSACTIONS ________________________________________


def database_status():
    """Showing account status."""
    print('\n' + '-' * 32, '  STATUS  ', '-' * 32)
    status = 'SELECT USD, EUR, GBP, PLN FROM balance'
    cursor.execute(status)
    print(colored(timer(), 'yellow'))
    for USD, EUR, GBP, PLN in cursor:
        print(colored(f'\nAccount balance: ', 'green'))
        print(f'USD = {USD}\n'
              f'EUR = {EUR}\n'
              f'GBP = {GBP}\n'
              f'PLN = {PLN}\n')
        print(colored('Checking account is in USD.', 'yellow'))
        menu()


def history():
    """Transfer history selection panel."""
    print('\n' + '-' * 25, '  TRANSACTIONS HISTORY  ', '-' * 25)
    print('Please enter correct ops:')
    print('(press 0 to return to the previous menu)\n')
    print('1) all history\n2) deposit\n3) withdraw\n4) exchange')
    while True:
        transaction = get_operation_number()
        if transaction == 1:
            full_history()
        elif transaction == 2:
            deposit_history()
        elif transaction == 3:
            withdraw_history()
        elif transaction == 4:
            exchange_history()
        elif transaction == 0:
            menu()
        else:
            print(colored('Please choose available ops.', 'red'))


def full_history():
    """Extract from the database all transfers history."""
    print('\n' + '-' * 30, '  FULL HISTORY  ', '-' * 28)
    transaction_history = 'SELECT * FROM transactions;'
    cursor.execute(transaction_history)
    results = cursor.fetchall()
    table = PrettyTable()
    for row in results:
        table.field_names = ['no', 'type', 'USD', 'EUR', 'GBP', 'PLN', 'date']
        table.add_row([f'{row[1]}', f'{row[2]}', f'{row[3]}', f'{row[4]}',
                       f'{row[5]}', f'{row[6]}', f'{row[7]}'])
    print(colored(table, 'yellow'))


def deposit_history():
    """Extract from the database deposit transfers."""
    print('\n' + '-' * 28, '  DEPOSIT HISTORY  ', '-' * 27)
    total_deposit = 'SELECT SUM(USD) FROM transactions WHERE type = "deposit"'
    deposit_operations = 'SELECT * FROM transactions WHERE type = "deposit"'
    cursor.execute(total_deposit)
    for USD in cursor:
        row = str(USD[0]).replace('None', '----')
        print(colored(f'TOTAL USD DEPOSIT: {row}\n', 'blue'))
    cursor.execute(deposit_operations)
    results = cursor.fetchall()
    table = PrettyTable()
    for row in results:
        table.field_names = ['no', 'type', 'USD', 'date']
        table.add_row([f'{row[1]}', f'{row[2]}', f'{row[3]}', f'{row[7]}'])
    print(colored(table, 'yellow'))


def withdraw_history():
    """Extract from the database withdraw transfers."""
    print('\n' + '-' * 28, '  WITHDRAW HISTORY  ', '-' * 26)
    total_withdraw = 'SELECT SUM(USD) FROM transactions WHERE type = "withdraw"'
    withdraw_operations = 'SELECT * FROM transactions WHERE type = "withdraw"'
    cursor.execute(total_withdraw)
    for USD in cursor:
        row = str(USD[0]).replace('None', '----')
        print(colored(f'TOTAL USD WITHDRAW: {row}\n', 'blue'))
    cursor.execute(withdraw_operations)
    table = PrettyTable()
    results = cursor.fetchall()
    for row in results:
        table.field_names = ['no', 'type', 'USD', 'date']
        table.add_row([f'{row[1]}', f'{row[2]}', f'{row[3]}', f'{row[7]}'])
    print(colored(table, 'yellow'))


def exchange_history():
    """Extract from the database withdraw transfers."""
    print('\n' + '-' * 28, '  EXCHANGE HISTORY  ', '-' * 26)
    exchange_operations = 'SELECT * FROM transactions WHERE type = "ex_with" OR type = "ex_depo"'
    cursor.execute(exchange_operations)
    table = PrettyTable()
    results = cursor.fetchall()
    for row in results:
        table.field_names = ['no', 'type', 'USD', 'EUR', 'GBP', 'PLN', 'date']
        table.add_row([f'{row[1]}', f'{row[2]}', f'{row[3]}', f'{row[4]}',
                       f'{row[5]}', f'{row[6]}', f'{row[7]}'])
    print(colored(table, 'yellow'))


# _________________________________________  CHANGE PIN  ________________________________________


def change_pin():
    """Checking the current PIN."""
    print('\n' + '-' * 30, '  CHANGE PIN  ', '-' * 30)
    print('Please enter your current PIN:',
          '\n(press 0 to return to the previous menu)\n')
    new = get_pin_number()
    if new == pin_database():
        print(colored('Correct', 'green'))
        new_pin()
    elif new == 0:
        menu()
    else:
        print(colored('It is not your current PIN.', 'red'))
        change_pin()


def new_pin():
    """Entering a new password."""
    print('\nPlease enter your new PIN: ')
    pin = get_pin_number()
    if pin != pin_database():
        new_pin_validator(pin)
    else:
        print(colored('You must enter another PIN.', 'red'))


def new_pin_validator(pin):
    """Checking if new PIN meet the criteria."""
    if pin in range(1000, 10000):
        pin_changer(pin)
        print(colored(f'Operation succeed, your new PIN is {pin}.', 'blue'))
        main()
    elif pin == 0:
        menu()
    else:
        print(colored('The new PIN does not meet the requirements.', 'red'))
        new_pin()


def pin_changer(current_pin):
    """Replacement of a PIN in the database."""
    pin_data = f'UPDATE pin SET PIN = {current_pin};'
    cursor.execute(pin_data)
    connection.commit()


def pin_database():
    """Reading the PIN from the database."""
    pin_data = 'SELECT PIN FROM pin;'
    cursor.execute(pin_data)
    for current_pin in cursor:
        code = current_pin[0]
        return code


# ________________________________________  MAIN FUNCTION _______________________________________


def main():
    current_pin = pin_database()
    welcome_panel(current_pin)


if __name__ == '__main__':
    main()

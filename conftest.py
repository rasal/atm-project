from config import data
import sqlite3
import pytest


@pytest.fixture(autouse=True)
def test_connection(request):
    connection = sqlite3.connect(data)
    connection.isolation_level = None
    connection.execute("BEGIN TRANSACTION")

    def finalize():
        connection.rollback()
        connection.close()

    request.addfinalizer(finalize)
    return connection

from pathlib import Path
import os


def data(source):
    """Finding objective root path directory."""
    current_dir = Path(__file__)
    root_dir = next(
        p for p in current_dir.parents
        if p.name == "atm-project")
    return os.path.join(root_dir, f"{source}")


class Config:
    @staticmethod
    def account_data(database):
        return database.fetch_data("accounts", "*")

    @staticmethod
    def balance_data(database):
        return database.fetch_data("balance", "*")

    @staticmethod
    def transaction_data(database):
        return database.fetch_data("transactions", "*")


config = Config()
data = data(r"database\atm_lite.db")

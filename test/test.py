#!/usr/bin/env python3
# Kivy modules broke tests in CLI/GUI for CI/CD
from main.data.database import Database
from main.ops.deposit import DepositWindow
from main.ops.withdraw import WithdrawWindow
from main.rest.ax_ops import AuxiliaryOperation
from freezegun import freeze_time
import datetime
import pytest
import requests
import json


date = datetime.date.today()
ENDPOINT = "https://api.exchangerate.host/"


@pytest.mark.parametrize("test_input_1, test_input_2, expected", [(1234567, 1234, True), (1, 1, False)])
def test1_get_user(test_input_1, test_input_2, expected, test_connection):
    assert Database(test_connection).validate_login(test_input_1, test_input_2) == expected


@pytest.mark.parametrize("account_number, password, expected", [(
        1234567, 1234, True), pytest.param(1, 1, None, marks=pytest.mark.xfail(reason="bug"))])
def test2_validate_login(account_number, password, expected, test_connection):
    assert Database(test_connection).validate_login(account_number, password) == expected


# DEPOSIT
@pytest.mark.parametrize("value, expected", [("1000.0", 1000.0), ("100001.0", None)])
def test3_deposit_checker(value, expected, test_connection):
    assert DepositWindow(Database(test_connection)).deposit_checker(value) == expected


@pytest.mark.parametrize("amount, expected", [("1000", "1000"), ("67890", "67890")])
def test4_successful_deposit(amount, expected):
    assert DepositWindow.successful_deposit(amount) == expected


# WITHDRAW
@pytest.mark.parametrize("value, expected", [("1000.0", 1000.0), ("100001.0", None)])
def test5_deposit_checker(value, expected, test_connection):
    assert WithdrawWindow(Database(test_connection)).withdraw_checker(value) == expected


@pytest.mark.parametrize("amount, expected", [("1000", "1000"), ("67890", "67890")])
def test6_successful_deposit(amount, expected):
    assert WithdrawWindow.successful_withdraw(amount) == expected


@pytest.mark.xfail
def test7_get_datetime():
    assert AuxiliaryOperation.get_datetime() == "16-04-2020"


@freeze_time("2022-01-01")
def test8_get_date():
    assert "2022" in AuxiliaryOperation.get_date()


def test9_previous_year():
    previous_year = int(date.strftime("%Y")) - 1
    assert str(previous_year) in AuxiliaryOperation.previous_year()


@pytest.mark.skip(reason="Testing skipped status")
def test10_previous_twelve_months():
    assert AuxiliaryOperation.previous_twelve_months() == "xxxxxxx"


@pytest.mark.parametrize("value, expected", [("0.7", 0.7), ("10.0", 10.0), ("1.1237", 1.12), ("234.99", 234.99),
                                             pytest.param("0.11", 0.21, marks=pytest.mark.xfail(reason='bug')),
                                             pytest.param(None, True, marks=pytest.mark.skip(reason='error'))])
def test11_amount_from_value(value, expected):
    assert AuxiliaryOperation.amount_from_value(value) == expected, "Test invalid input assertion."


@pytest.mark.parametrize("amount, expected", [(1000, True), (678900000000, False)])
def test12_available_funds(amount, expected, test_connection):
    assert Database(test_connection).available_funds(amount) == expected


def test13_api_response_status_code_200():
    response = requests.get(ENDPOINT)
    assert response.status_code == 200


def test14_api_response_from_currency():
    path = "convert?from=USD&to=EUR"
    response = requests.get(ENDPOINT + path)
    data = json.loads(response.text)
    query = data["query"]["from"]
    print("", query)
    assert query == "USD"
